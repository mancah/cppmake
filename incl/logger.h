#ifndef LOGGER_H_
#define LOGGER_H_

#include <atomic>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "scope_benchmark.h"

class Logger {
  public:
    static inline Logger& GetInstance() {
        static Logger instance;
        return instance;
    }

    void Log(const std::string& module, const std::string& message,
             const std::string& level = "INFO") {
        std::lock_guard<std::mutex> lock(log_mutex_);

        log_counter_++;
        log_messages_ << GetDateTime() << " - "
                      << "[" << module << "] - "
                      << "[" << std::setw(7) << std::left << level << "] - "
                      << message << "\n";

        if (log_counter_ >= buffer_size_) {
            FlushLog();
        }
    }

    ~Logger() {
        try {
            stop_flush_thread_ = true;
            if (flush_thread_.joinable()) {
                flush_thread_.join();
            }

            std::lock_guard<std::mutex> lock(log_mutex_);
            FlushLog();
        } catch (...) {
            std::cout << "Exception in ~Logger()" << std::endl;
        }
    }

    void SetBufferSize(size_t size) { buffer_size_ = size; }

  private:
    Logger() {
        file_name_ = "log.txt";
        file_size_ = GetFileSizeMB();
        if (file_size_ > MAX_FILE_SIZE_MB) {
            RenameAndOpenNewFile();
        } else {
            log_file_.open(file_name_, std::ios_base::app);
        }

        last_flush_time_ = std::chrono::steady_clock::now();
        stop_flush_thread_ = false;
        flush_thread_ = std::thread([this]() {
            while (!stop_flush_thread_) {
                auto now = std::chrono::steady_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(now - last_flush_time_);
                if (duration.count() >= 500) {
                    FlushLog();
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
            }
        });
    }

    // Rule of 5
    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;
    Logger(Logger&&) = delete;
    Logger& operator=(Logger&&) = delete;

    uint64_t GetFileSizeMB() {
        std::ifstream file(file_name_, std::ios::binary | std::ios::ate);
        if(!file.good()) {
            return 0;
        }

        return file.tellg();
    }

    void RenameAndOpenNewFile() {
        if (log_file_.is_open()) {
            log_file_.close();
        }

        std::string new_filename = GetDateTimeForFileName() + "_" + file_name_;
        std::rename(file_name_.c_str(), new_filename.c_str());
        log_file_.open("log.txt", std::ios::out | std::ios::app);
        file_size_ = 0;
    }

    void FlushLog() {
        last_flush_time_ = std::chrono::steady_clock::now();
        if (log_counter_ < buffer_size_) {
            return;
        }

        file_size_ += log_messages_.str().size();
        log_file_ << log_messages_.str() << std::flush;
        log_messages_ = {};
        log_counter_ = 0;

        flush_counter_++;
        if (flush_counter_ > flush_checker_) {
            flush_counter_ = 0;
            if (file_size_ > MAX_FILE_SIZE_MB) {
                RenameAndOpenNewFile();
            }
        }
    }

    std::string GetDateTimeForFileName() {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        std::ostringstream oss;
        oss << std::put_time(&tm, "%Y%m%d_%H%M%S");
        return oss.str();
    }

    std::string GetDateTime() {
        auto now = std::chrono::system_clock::now();
        auto now_c = std::chrono::system_clock::to_time_t(now);
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                      now.time_since_epoch()) %
                  1000;
        std::stringstream date_time;
        date_time << std::put_time(std::localtime(&now_c), "%d %B %Y %I:%M:%S")
                  << '.' << std::setfill('0') << std::setw(3) << ms.count()
                  << " " << std::put_time(std::localtime(&now_c), "%p");
        return date_time.str();
    }

    std::ofstream log_file_;
    std::mutex log_mutex_;
    size_t log_counter_ = 0;
    std::ostringstream log_messages_;
    size_t buffer_size_ = 100;
    const uint64_t MAX_FILE_SIZE_MB = 30 * 1024 * 1024;
    uint16_t flush_counter_ = 0;
    uint16_t flush_checker_ = 100;

    std::string file_name_;

    std::chrono::time_point<std::chrono::steady_clock> last_flush_time_;
    std::atomic<bool> stop_flush_thread_;
    std::thread flush_thread_;
    std::atomic<size_t> file_size_;  // New member variable to keep track of the file size
};

#endif  // LOGGER_H_

// main.cpp to test this
// #include <thread>
//
// #include "logger.h"
// #include "scope_benchmark.h"
//
// void log_operation(int num) {
//     for (int i = 0; i < num; ++i) {
//         Logger::GetInstance().Log("SubFunction", "This is a log message2",
//                                   "WARNING");
//         Logger::GetInstance().Log("SubFunction", "This is a log message3",
//                                   "ERROR");
//         Logger::GetInstance().Log("SubFunction", "This is a log message4",
//                                   "DEBUG");
//         Logger::GetInstance().Log("SubFunction", "This is a log message5",
//                                   "INFO");
//     }
// }
//
// int main(int argc, char* argv[]) {
//     ScopeBenchmark scope_benchmark("MainFunction");
//     int num = 1000000;
//     if (argc > 1) {
// 		num = std::atoi(argv[1]);
//     }
//
//     // Create multiple threads
//     std::thread t1(log_operation, num);
//     std::thread t2(log_operation, num);
//     std::thread t3(log_operation, num);
//     std::thread t4(log_operation, num);
//     std::thread t5(log_operation, num);
//
//     // Wait for all threads to finish
//     t1.join();
//     t2.join();
//     t3.join();
//     t4.join();
//     t5.join();
//
//     return 0;
// }

