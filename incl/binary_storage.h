#include <fstream>
#include <vector>
#include <filesystem>

#include "scope_benchmark.h"

class MyClass {
    std::string filename = "./data.bin";

public:
    std::vector<uint64_t> data;
    MyClass() {
        ScopeBenchmark bench("Loading data");
        // Load data from file if it exists
        if (std::filesystem::exists(filename)) {
            std::ifstream input(filename, std::ios::binary);
            input.seekg(0, std::ios::end);
            size_t size = input.tellg() / sizeof(uint64_t);
            input.seekg(0, std::ios::beg);

            data.reserve(size); // reserve memory for the data

            // Read the file in chunks
            uint64_t buffer;
            while(input.read(reinterpret_cast<char*>(&buffer), sizeof(uint64_t))) {
                data.push_back(buffer);
            }

            std::cout << "Initial size: " << data.size() << std::endl;
        } else {
            std::cout << "File does not exist" << std::endl;
        }
    }

    ~MyClass() {
        ScopeBenchmark bench("Saving data");
        // Save data to file (overwrites if file already exists)
        std::ofstream output(filename, std::ios::binary | std::ios::trunc);
        std::cout << "Final size: " << data.size() << std::endl;

        for(const auto& value : data) {
            output.write(reinterpret_cast<const char*>(&value), sizeof(uint64_t));
        }
    }

    void addData(uint64_t value) {
        data.push_back(value);
    }
};

