#include <QMap>
#include <QSet>

class TokenManager {
public:
    TokenManager() : nextTokenID(1) {}

    int getToken() {
        int tokenID;

        if (availableTokens.isEmpty()) {
            tokenID = nextTokenID;
            ++nextTokenID;
        } else {
            auto it = availableTokens.begin();
            tokenID = *it;
            availableTokens.erase(it);
        }

        return tokenID;
    }

    void releaseToken(int tokenID) {
        availableTokens.insert(tokenID);
    }

private:
    int nextTokenID;
    QSet<int> availableTokens;
};

// Example of using TokenManager
int main() {
    TokenManager tokenManager;

    int token1 = tokenManager.getToken();
    int token2 = tokenManager.getToken();

    // Use token1 and token2...

    // Release token1, so it can be reused
    tokenManager.releaseToken(token1);
    tokenManager.releaseToken(token1);

    // Get a new token
    int token3 = tokenManager.getToken();

    return 0;
}

