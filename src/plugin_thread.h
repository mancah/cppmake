#ifndef PLUGIN_THREAD_H
#define PLUGIN_THREAD_H

#include <QDebug>
#include <QJsonObject>
#include <QObject>
#include <QThread>
#include <memory>

using RespCallback = std::function<void(const QJsonObject &)>;

struct RequestData {
    RespCallback resp_cb;
    bool notification;
    QJsonObject request;
};

class Plugin : public QObject {
    Q_OBJECT
  public:
    Plugin() = default;
    bool Process(const QJsonObject &req_json);

  signals:
    void SendBytes(const QString &client_id, const QJsonObject &resp);
    void SendLog(int32_t priority, const QString &function_name,
                 const QString &format);
    void SendAudit(const QJsonObject &payload);
};

class PluginThread : public QObject {
    Q_OBJECT

  public:
    PluginThread(const QString &plugin_name,
                 QHash<QString, RequestData> &req_map);
    Plugin *GetPlugin();

  signals:
    void SendBytes(const QString &client_id, const QJsonObject &resp);
    void SendLog(int32_t priority, const QString &function_name,
                 const QString &format);
    void SendAudit(const QJsonObject &payload);
    void NewRequest(const QJsonObject &req);
    void PendingCallback(RespCallback &callback, const QJsonObject &resp);

  private slots:
    void CallbackRunner(RespCallback &callback, const QJsonObject &resp);

  private:
    void HandleRequest(const QJsonObject &req);
    std::unique_ptr<Plugin> plugin_;
    QHash<QString, RequestData> &req_map_;
    QString plugin_name_;
    QThread thread_;
};

inline PluginThread::PluginThread(const QString &plugin_name,
                                  QHash<QString, RequestData> &req_map)
    : req_map_(req_map), plugin_name_(plugin_name)
{
    moveToThread(&thread_);
    connect(&thread_, &QThread::started, [this]() {
        plugin_ = std::make_unique<Plugin>();
        connect(plugin_.get(), &Plugin::SendBytes, this,
                &PluginThread::SendBytes, Qt::QueuedConnection);
        connect(plugin_.get(), &Plugin::SendLog, this, &PluginThread::SendLog,
                Qt::QueuedConnection);
        connect(plugin_.get(), &Plugin::SendAudit, this,
                &PluginThread::SendAudit, Qt::QueuedConnection);
        connect(this, &PluginThread::NewRequest, this,
                &PluginThread::HandleRequest, Qt::QueuedConnection);
        connect(this, &PluginThread::PendingCallback, this,
                &PluginThread::CallbackRunner, Qt::QueuedConnection);
    });

    thread_.start();
}

inline void PluginThread::HandleRequest(const QJsonObject &req)
{
    if (!plugin_->Process(req)) {
        QJsonObject resp{
            {"uid", req["uid"]},
            {"requestObjId", req["requestObjId"]},
            {"opCode", req["opCode"]},
            {"state", "StUnknown"},
        };

        SendBytes(req["clientId"].toString(), resp);
    }
}

inline Plugin *PluginThread::GetPlugin() { return plugin_.get(); }

inline void PluginThread::CallbackRunner(RespCallback &callback,
                                         const QJsonObject &resp)
{
    callback(resp);
}

inline bool Plugin::Process(const QJsonObject &req_json)
{
    qDebug() << "Processing request: " << req_json;
    return true;
}

#endif  // PLUGIN_THREAD_H

