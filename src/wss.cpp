#include "wss.h"
#include <QDateTime>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonParseError>
#include <QSslConfiguration>
#include <QTextStream>

WebSocketClient::WebSocketClient(QUrl url, QObject *parent) :
    QObject(parent), url_(url) {
    connect(&socket_, &QWebSocket::disconnected, this, &WebSocketClient::onDisconnected);
    connect(&socket_, &QWebSocket::connected, this, &WebSocketClient::onConnected);
    connect(&socket_, &QWebSocket::textMessageReceived, this, &WebSocketClient::onTextMessageReceived);

    timer_.reset(new QTimer());
    connect(timer_.data(), &QTimer::timeout, this, &WebSocketClient::reConnect);

    QSslConfiguration sslconfig_;
    sslconfig_.setPeerVerifyMode(QSslSocket::VerifyNone);

    socket_.setSslConfiguration(sslconfig_);
    socket_.ignoreSslErrors();
    timer_->start(1000);
}

void WebSocketClient::reConnect() {
    socket_.open(url_);
}

void WebSocketClient::onConnected() {
    timer_->stop();
    qDebug() << "------------------------------------------------";
    qDebug().noquote() << QString("{[\"Connected to %1\"]}").arg(url_.toString());
    qDebug() << "------------------------------------------------";
    is_up = true;
}

void WebSocketClient::onDisconnected() {
    timer_->start(1000);
    qDebug() << "------------------------------------------------";
    qDebug().noquote() << QString("{[\"Disconnected from server\"]}");
    qDebug() << "------------------------------------------------";
    is_up = false;
}

void WebSocketClient::onTextMessageReceived(QString data) {
    QStringList header = data.left(26).split(QLatin1Char('|'));
    long uid = header[0].toLong();
    long index = header[1].toLong();
    long total = header[2].toLong();

    if (req_map_.count(uid) == 0) {
        req_map_[uid] = {};
    }

    auto& req_data = req_map_[uid].data;
    req_data[index] = data.mid(27).toUtf8();

    if (index == total) {
        QByteArray bytes = "";
        for (int i = 1; i <= total; i++) {
            bytes += req_data[i];
        }

        auto time_taken =
            std::chrono::duration_cast<std::chrono::microseconds>(
                std::chrono::high_resolution_clock::now() - req_map_[uid].start)
                .count();

        req_map_.erase(uid);
        qDebug() << total << " packets for uid "
                 << uid << " with total size of: " << bytes.size() / 1024.0 / 1024.0
                 << "MB in " << QString("%1 ms").arg(time_taken / 1000.0, 0, 'f', 2)
                 << "\n";
    }
}

void WebSocketClient::Send(QJsonObject &data) {
    data["uid"] = uid_counter_++;
    QJsonDocument doc(data);
    socket_.sendTextMessage(doc.toJson());
    req_map_[uid_counter_ - 1] = {};
}

void WebSocketClient::Disconnect() {
    is_up = false;
    socket_.close();
}

