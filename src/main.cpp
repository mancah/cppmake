#include <iostream>
#include <string>
#include <sstream>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QByteArray>

class JsonArray;

class JsonObject {
private:
    std::ostringstream jsonString;
    bool isFirstElement;

public:
    JsonObject() : isFirstElement(true) {
        jsonString << "{";
    }

    template <typename T>
    void insert(const std::string& key, const T& value) {
        if (!isFirstElement) {
            jsonString << ",";
        }
        jsonString << "\"" << key << "\":" << value;
        isFirstElement = false;
    }

    void insert(const std::string& key, const std::string& value) {
        if (!isFirstElement) {
            jsonString << ",";
        }
        jsonString << "\"" << key << "\":\"" << value << "\"";
        isFirstElement = false;
    }

    void insert(const std::string& key, const char* value) {
        if (!isFirstElement) {
            jsonString << ",";
        }
        jsonString << "\"" << key << "\":\"" << value << "\"";
        isFirstElement = false;
    }

    void insert(const std::string& key, JsonArray& jsonArray);
    void insert(const std::string& key, JsonObject& jsonObject) {
        if (!isFirstElement) {
            jsonString << ",";
        }
        jsonString << "\"" << key << "\":" << jsonObject.getJsonString().data();
        isFirstElement = false;
    }

    QByteArray getJsonString() {
        jsonString << "}";
        return QByteArray::fromStdString(jsonString.str());
    }
};

class JsonArray {
private:
    std::ostringstream jsonArrayString;
    bool isFirstElement;

public:
    JsonArray() : isFirstElement(true) {
        jsonArrayString << "[";
    }

    template <typename T>
    void insert(const T& value) {
        if (!isFirstElement) {
            jsonArrayString << ",";
        }
        jsonArrayString << value;
        isFirstElement = false;
    }

    void insert(const std::string& value) {
        if (!isFirstElement) {
            jsonArrayString << ",";
        }
        jsonArrayString << "\"" << value << "\"";
        isFirstElement = false;
    }

    void insert(const char* value) {
        if (!isFirstElement) {
            jsonArrayString << ",";
        }
        jsonArrayString << "\"" << value << "\"";
        isFirstElement = false;
    }

    void insert(JsonObject& jsonObject) {
        if (!isFirstElement) {
            jsonArrayString << ",";
        }
        jsonArrayString << jsonObject.getJsonString().data();
        isFirstElement = false;
    }

    QByteArray getJsonArrayString() {
        jsonArrayString << "]";
        return QByteArray::fromStdString(jsonArrayString.str());
    }
};

void JsonObject::insert(const std::string& key, JsonArray& jsonArray)
{
    if (!isFirstElement) {
        jsonString << ",";
    }
    jsonString << "\"" << key << "\":" << jsonArray.getJsonArrayString().data();
    isFirstElement = false;
}

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

class ScopeBenchmark {
  public:
    ScopeBenchmark(std::string function_name = "", int new_line = 1) :
        m_start(std::chrono::high_resolution_clock::now()),
        m_function_name(function_name)
    { }

    ~ScopeBenchmark()
    {
        auto end = std::chrono::high_resolution_clock::now();
        auto time_taken =
            std::chrono::duration_cast<std::chrono::microseconds>(end - m_start)
                .count();

        std::cout << std::setw(16) << std::left << "bench_test | "
                  << std::setw(40) << std::left << m_function_name.c_str()
                  << " >>>>>>> " << std::setw(9) << std::right << time_taken
                  << " us" << " (" << time_taken / 1000 << " ms)";

        std::cout << std::endl;
    }

  private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
    std::string m_function_name;
};

void TestJsonClasses()
{
    QByteArray jsonString;
    {
        ScopeBenchmark _1(__FUNCTION__);
        JsonObject result;
        for (auto i = 0; i < 1000000; i++) {
            JsonObject jsonObject;
            jsonObject.insert("name", "John");
            jsonObject.insert("age", 25);
            jsonObject.insert("isStudent", true);

            JsonArray jsonArray;
            jsonArray.insert("apple");
            jsonArray.insert("banana");
            jsonArray.insert("cherry");

            jsonObject.insert("fruits", jsonArray);
            result.insert(std::to_string(i), jsonObject);
        }

        jsonString = result.getJsonString();
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

void TestQJsonClasses()
{
    QByteArray jsonString;
    {
        ScopeBenchmark _1(__FUNCTION__);
        QJsonObject result;
        for (int i = 0; i < 1000000; i++) {
            QJsonObject jsonObject;
            jsonObject.insert("name", "John");
            jsonObject.insert("age", 25);
            jsonObject.insert("isStudent", true);

            QJsonArray jsonArray;
            jsonArray.append("apple");
            jsonArray.append("banana");
            jsonArray.append("cherry");

            jsonObject.insert("fruits", jsonArray);
            result.insert(QString::number(i), jsonObject);
        }

        QJsonDocument doc(result);
        jsonString = doc.toJson(QJsonDocument::Compact);
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

int main (int argc, char *argv[])
{
    TestJsonClasses();
    TestQJsonClasses();
    return 0;
}

