#ifndef WEBSOCKETCLIENT_H
#define WEBSOCKETCLIENT_H

#include <QObject>
#include <QWebSocket>
#include <QScopedPointer>
#include <QTimer>
#include <QSocketNotifier>
#include <QSemaphore>
#include <QMutex>
#include <unordered_map>
#include <chrono>

#include <iostream>

class WebSocketClient : public QObject {
    Q_OBJECT

public:
    WebSocketClient(QUrl url, QObject *parent = nullptr);
    void Send(QJsonObject &data);
    void Disconnect();
    bool IsConnected() const { return is_up; }

public slots:
    void reConnect();
    void onConnected();
    void onDisconnected();
    void onTextMessageReceived(QString data);

  private:
    struct Request {
        std::unordered_map<long, QByteArray> data;
        std::chrono::time_point<std::chrono::high_resolution_clock> start = std::chrono::high_resolution_clock::now();
    };

    QWebSocket socket_;
    QUrl url_;
    bool is_up = false;
    qint64 uid_counter_ = 0;
    QScopedPointer<QTimer> timer_;
    std::unordered_map<long, Request> req_map_;
};

#endif // WEBSOCKETCLIENT_H

