
#include <iostream>
#include <vector>
#include <algorithm>
#include <chrono>


double findUsingBrackets(const std::vector<bool>& vec) {
    auto start = std::chrono::high_resolution_clock::now();
    bool found = false;
    for (size_t i = 0; i < vec.size(); ++i) {
        if (!vec[i]) {
            found = true;
            break;
        }
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> duration = end - start;
    return duration.count();
}

double findUsingAnyOf(const std::vector<bool>& vec) {
    auto start = std::chrono::high_resolution_clock::now();
    bool found = std::any_of(vec.begin(), vec.end(), [](bool v) { return !v; });
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> duration = end - start;
    return duration.count();
}

int main() {
    // Create a large vector of bools
    std::vector<bool> vec(1000000, true);
    vec[500000] = false; // Insert a false value in the middle

    // Measure time using []
    double duration1 = findUsingBrackets(vec);

    // Measure time using std::any_of
    double duration2 = findUsingAnyOf(vec);

    // Print results
    std::cout << "Time using          []: " << duration1 << " seconds\n";
    std::cout << "Time using std::any_of: " << duration2 << " seconds\n";

    return 0;
}

