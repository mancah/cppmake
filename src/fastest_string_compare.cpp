#include <algorithm>
#include <cctype>
#include <chrono>
#include <cstring>
#include <iostream>
#include <vector>

#include <cctype>
#include <regex>

inline bool IsMatchingStringCaseSensitiveWindowsRegex(const std::string &str, const std::string &pattern) {
    // Convert the wildcard pattern to a regex pattern
    std::string regexPattern;
    regexPattern.reserve(pattern.size() * 2); // Reserve space to avoid multiple allocations

    for (char ch : pattern) {
        switch (ch) {
            case '*':
                regexPattern += ".*";
                break;
            case '?':
                regexPattern += '.';
                break;
            default:
                regexPattern += ch;
                break;
        }
    }

    std::regex re(regexPattern);
    return std::regex_match(str, re);
}

int32_t strmatch_case_insensitive(const char *pattern, const char *string) {
    const char *psave = nullptr, *ssave = nullptr;

    if (pattern == nullptr) return 1;

    while (true) {
        while (tolower(*pattern) == tolower(*string)) {
            if (*pattern == '\0') return 1;
            pattern++;
            string++;
        }

        if (*string != '\0' && *pattern == '?') {
            pattern++;
            string++;
        } else if (*pattern == '*') {
            psave = ++pattern;
            ssave = string;
        } else if (ssave) {
            string = ++ssave;
            pattern = psave;
        } else {
            return 0;
        }
    }
}

int32_t strmatch(const char *pattern, const char *string) {
    char *psave, *ssave;
    psave = ssave = ((char *)0);

    /* Blank pattern always matches. */
    if (pattern == nullptr) return 1;

    while (1) {
        /* Skip first */
        for (; *pattern == *string; pattern++, string++)
            if (*pattern == '\0') /* End of pattern, succeed. */
                return 1;

        if (*string != '\0' && *pattern == '?') {
            /* If '?', let it match */
            pattern++;
            string++;
        } else {
            if (*pattern == '*') {
                /* If '*', remember where we saw it and let it match 0 chars */
                psave = (char *)++pattern;
                ssave = (char *)string;
            } else {
                if (ssave != ((char *)0) && *ssave != '\0') {
                    /*
                     * If not at end and have seen a star skip 1 char from
                     * string and back up pattern
                     */
                    string = ++ssave;
                    pattern = psave;
                } else
                    /* Otherwise just fail */
                    return 0;
            }
        }
    }
}


 //Case-insensitive version
inline bool IsMatchingStringCaseInsensitive(const std::string &str, const std::string &pattern) {
    auto m = str.size();
    auto n = pattern.size();

    // Convert both strings to lowercase
    char lowerStr[m + 1];
    char lowerPattern[n + 1];
    std::transform(str.begin(), str.end(), lowerStr, ::tolower);
    std::transform(pattern.begin(), pattern.end(), lowerPattern, ::tolower);
    lowerStr[m] = '\0';
    lowerPattern[n] = '\0';

    // Create two arrays to store results of subproblems
    bool prev[n + 1];
    bool curr[n + 1];

    // Base case: empty pattern matches empty string
    std::memset(prev, 0, sizeof(prev));
    prev[0] = true;

    // Handle patterns with '*' at the beginning
    for (auto j = 1; j <= n; ++j) {
        if (lowerPattern[j - 1] == '*') {
            prev[j] = prev[j - 1];
        }
    }

    // Fill the table
    for (auto i = 1; i <= m; ++i) {
        curr[0] = false; // Empty pattern cannot match non-empty string
        bool allFalse = true; // Flag to check if all entries in curr are false
        for (auto j = 1; j <= n; ++j) {
            if (lowerPattern[j - 1] == '*') {
                // '*' can match zero or more characters
                curr[j] = curr[j - 1] || prev[j];
            } else if (lowerPattern[j - 1] == lowerStr[i - 1] || lowerPattern[j - 1] == '?') {
                // Match current characters or '?' wildcard
                curr[j] = prev[j - 1];
            } else {
                curr[j] = false;
            }
            if (curr[j]) {
                allFalse = false; // If any entry is true, set flag to false
            }
        }
        if (allFalse) {
            return false; // Early exit if no possible match
        }
        std::memcpy(prev, curr, sizeof(prev));
    }

    return prev[n];
}

 //Case-sensitive version
inline bool IsMatchingStringCaseSensitive(const std::string &str, const std::string &pattern) {
    auto m = str.size();
    auto n = pattern.size();

    // Create two arrays to store results of subproblems
    bool prev[n + 1];
    bool curr[n + 1];

    // Base case: empty pattern matches empty string
    std::memset(prev, 0, sizeof(prev));
    prev[0] = true;

    // Handle patterns with '*' at the beginning
    for (auto j = 1; j <= n; ++j) {
        if (pattern[j - 1] == '*') {
            prev[j] = prev[j - 1];
        }
    }

    // Fill the table
    for (auto i = 1; i <= m; ++i) {
        curr[0] = false; // Empty pattern cannot match non-empty string
        bool allFalse = true; // Flag to check if all entries in curr are false
        for (auto j = 1; j <= n; ++j) {
            if (pattern[j - 1] == '*') {
                // '*' can match zero or more characters
                curr[j] = curr[j - 1] || prev[j];
            } else if (pattern[j - 1] == str[i - 1] || pattern[j - 1] == '?') {
                // Match current characters or '?' wildcard
                curr[j] = prev[j - 1];
            } else {
                curr[j] = false;
            }
            if (curr[j]) {
                allFalse = false; // If any entry is true, set flag to false
            }
        }
        if (allFalse) {
            return false; // Early exit if no possible match
        }
        std::memcpy(prev, curr, sizeof(prev));
    }

    return prev[n];
}

inline bool IsMatchingString(const std::string &str, const std::string &pattern) {
    auto m = str.size();
    auto n = pattern.size();

    // Create two arrays to store results of subproblems
    std::vector<bool> prev(n + 1, false);
    std::vector<bool> curr(n + 1, false);

    // Base case: empty pattern matches empty string
    prev[0] = true;

    // Handle patterns with '*' at the beginning
    for (auto j = 1; j <= n; ++j) {
        if (pattern[j - 1] == '*') {
            prev[j] = prev[j - 1];
        }
    }

    // Fill the table
    for (auto i = 1; i <= m; ++i) {
        curr[0] = false; // Empty pattern cannot match non-empty string
        bool allFalse = true; // Flag to check if all entries in curr are false
        for (auto j = 1; j <= n; ++j) {
            if (pattern[j - 1] == '*') {
                // '*' can match zero or more characters
                curr[j] = curr[j - 1] || prev[j];
            } else if (pattern[j - 1] == str[i - 1] || pattern[j - 1] == '?') {
                // Match current characters or '?' wildcard
                curr[j] = prev[j - 1];
            } else {
                curr[j] = false;
            }
            if (curr[j]) {
                allFalse = false; // If any entry is true, set flag to false
            }
        }
        if (allFalse) {
            return false; // Early exit if no possible match
        }
        prev = curr;
    }

    return prev[n];
}

inline bool IsMatchingStringCaseInsensitiveWindows(const std::string &str, const std::string &pattern) {
    auto m = str.size();
    auto n = pattern.size();

    if (str == pattern) {
        return true;
    }

    // Convert both strings to lowercase
    char* lowerStr = new char[m + 1];
    char* lowerPattern = new char[n + 1];
    for (size_t i = 0; i < m; ++i) {
        lowerStr[i] = std::tolower(str[i]);
    }
    lowerStr[m] = '\0';
    for (size_t j = 0; j < n; ++j) {
        lowerPattern[j] = std::tolower(pattern[j]);
    }
    lowerPattern[n] = '\0';

    // Create a single array to store results of subproblems
    bool* dp = new bool[n + 1];
    std::memset(dp, 0, (n + 1) * sizeof(bool));

    // Base case: empty pattern matches empty string
    dp[0] = true;

    // Handle patterns with '*' at the beginning
    for (size_t j = 1; j <= n; ++j) {
        if (lowerPattern[j - 1] == '*') {
            dp[j] = dp[j - 1];
        }
    }

    // Fill the table
    for (size_t i = 1; i <= m; ++i) {
        bool prev = dp[0];
        dp[0] = false; // Empty pattern cannot match non-empty string
        for (size_t j = 1; j <= n; ++j) {
            bool temp = dp[j];
            if (lowerPattern[j - 1] == '*') {
                dp[j] = dp[j - 1] || dp[j];
            } else if (lowerPattern[j - 1] == lowerStr[i - 1] || lowerPattern[j - 1] == '?') {
                dp[j] = prev;
            } else {
                dp[j] = false;
            }
            prev = temp;
        }
    }

    bool result = dp[n];
    delete[] lowerStr;
    delete[] lowerPattern;
    delete[] dp;
    return result;
}

inline bool IsMatchingStringCaseSensitiveWindows(const std::string &str, const std::string &pattern) {
    auto m = str.size();
    auto n = pattern.size();

    // Create a single array to store results of subproblems
    bool* dp = new bool[n + 1];
    std::memset(dp, 0, (n + 1) * sizeof(bool));

    // Base case: empty pattern matches empty string
    dp[0] = true;

    // Handle patterns with '*' at the beginning
    for (size_t j = 1; j <= n; ++j) {
        dp[j] = dp[j - 1] && (pattern[j - 1] == '*');
    }

    // Fill the table
    for (size_t i = 1; i <= m; ++i) {
        bool prev = dp[0];
        dp[0] = false; // Empty pattern cannot match non-empty string
        for (size_t j = 1; j <= n; ++j) {
            bool temp = dp[j];
            if (pattern[j - 1] == '*') {
                dp[j] = dp[j - 1] || dp[j];
            } else {
                dp[j] = (pattern[j - 1] == str[i - 1] || pattern[j - 1] == '?') && prev;
            }
            prev = temp;
        }
    }

    bool result = dp[n];
    delete[] dp;
    return result;
}

int main() {
    std::string str = "aBcDeFgHiJkLmNoPqRsTuVwXyZ";
    std::string pattern = "*b*d*f*h*j*l*n*p*r*t*v*x*z";
    static constexpr auto kTestCount = 1000000;

    // Benchmark case-insensitive function
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        IsMatchingStringCaseInsensitive(str, pattern);
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Case-insensitive function took: " << elapsed.count() << " seconds\n";

    // Benchmark case-sensitive function
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        IsMatchingStringCaseSensitive(str, pattern);
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Case-sensitive function took: " << elapsed.count() << " seconds\n";

    // Benchmark case-insensitive function for Windows
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        IsMatchingStringCaseInsensitiveWindows(str, pattern);
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Case-insensitive Windows function took: " << elapsed.count() << " seconds\n";

    // Benchmark case-sensitive function for Windows
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        IsMatchingStringCaseSensitiveWindows(str, pattern);
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Case-sensitive Windows function took: " << elapsed.count() << " seconds\n";

    // Benchmark case-sensitive function for Windows
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        strmatch(str.c_str(), pattern.c_str());
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "strmatch function took: " << elapsed.count() << " seconds\n";

    // Benchmark case-sensitive function for Windows
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        strmatch_case_insensitive(str.c_str(), pattern.c_str());
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "strmatch_case_insensitive function took: " << elapsed.count() << " seconds\n";


    // Benchmark case-sensitive function for Windows
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < kTestCount; ++i) {
        IsMatchingStringCaseSensitiveWindowsRegex(str, pattern);
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "IsMatchingStringCaseSensitiveWindowsRegex function took: " << elapsed.count() << " seconds\n\n";

    return 0;
}




