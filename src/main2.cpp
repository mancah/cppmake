#include <algorithm>
#include <iostream>
#include <vector>

struct TagRow {
    int tag_id;

    TagRow(int tag_id) : tag_id(tag_id) {}
};

std::vector<TagRow*> findCommonElements(
    const std::vector<std::vector<TagRow*>*>& vecs)
{
    if (vecs.empty()) return {};

    // Step 1: Find the smallest vector
    auto min_vec_it = std::min_element(
        vecs.begin(), vecs.end(),
        [](const std::vector<TagRow*>* a, const std::vector<TagRow*>* b) {
            return a->size() < b->size();
        });

    std::vector<TagRow*> master_map = **min_vec_it;

    // Step 2: Count occurrences of each element in the smallest vector
    for (const auto& vec : vecs) {
        if (vec == *min_vec_it) continue;

        std::vector<TagRow*> new_master_map;
        std::copy_if(
            master_map.begin(), master_map.end(),
            std::back_inserter(new_master_map), [&vec](const TagRow* tag) {
                return std::find(vec->begin(), vec->end(), tag) != vec->end();
            });
        master_map = std::move(new_master_map);
    }

    return master_map;
}

int main()
{
    auto tag1 = new TagRow{1};
    auto tag2 = new TagRow{2};
    auto tag3 = new TagRow{3};
    auto tag4 = new TagRow{4};
    auto tag5 = new TagRow{5};
    auto tag6 = new TagRow{6};
    auto tag7 = new TagRow{7};

    std::vector<TagRow*> vec1 = {tag1, tag3, tag4};
    std::vector<TagRow*> vec2 = {tag6, tag7, tag3, tag4, tag5};
    std::vector<TagRow*> vec3 = {tag6, tag3, tag4, tag7};

    std::vector<std::vector<TagRow*>*> vecs = {&vec1, &vec2, &vec3};

    std::vector<TagRow*> common_elements = findCommonElements(vecs);

    std::cout << "Common elements: ";
    for (auto elem : common_elements) {
        std::cout << elem->tag_id << " ";
    }
    std::cout << std::endl;

    return 0;
}


