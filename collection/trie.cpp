#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <limits>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "scope_benchmark.h"

class TrieNode {
  public:
    std::unordered_map<char, std::shared_ptr<TrieNode>> children;
    std::string                                         word;
    TrieNode() : word("") {}
};

class Trie {
  public:
    std::shared_ptr<TrieNode> root;
    Trie() : root(std::make_shared<TrieNode>()) {}

    void insert(std::string word)
    {
        std::shared_ptr<TrieNode> node = root;
        for (char c : word) {
            if (!node->children.count(c)) {
                node->children[c] = std::make_shared<TrieNode>();
            }
            node = node->children[c];
        }
        node->word = word;
    }

    void fuzzySearchUtil(std::shared_ptr<TrieNode> root,
                         std::string               str,
                         std::string               curr,
                         std::vector<std::string> &results,
                         bool                      caseSensitive,
                         int                       result_limit)
    {
        if (results.size() >= result_limit) {
            return;
        }

        if (!root->word.empty()) {
            std::string compareStr = caseSensitive ? curr : toLower(curr);
            if (compareStr.find(str) != std::string::npos) {
                results.push_back(root->word);
            }
        }

        for (auto it : root->children) {
            fuzzySearchUtil(it.second,
                            str,
                            curr + it.first,
                            results,
                            caseSensitive,
                            result_limit);
        }
    }
    std::vector<std::string> fuzzySearch(std::string str, int result_limit)
    {
        std::vector<std::string> results;
        bool                     caseSensitive = false;
        for (char c : str) {
            if (isupper(c)) {
                caseSensitive = true;
                break;
            }
        }
        std::string compareStr = caseSensitive ? str : toLower(str);
        fuzzySearchUtil(root,
                        compareStr,
                        "",
                        results,
                        caseSensitive,
                        result_limit);

        SortByPatternPosition(results, str, caseSensitive);

        return results;
    }

  private:
    std::string toLower(const std::string &str)
    {
        std::string lowerStr = str;
        std::transform(lowerStr.begin(),
                       lowerStr.end(),
                       lowerStr.begin(),
                       ::tolower);
        return lowerStr;
    }

    void SortByPatternPosition(std::vector<std::string> &strings,
                               const std::string        &pattern,
                               bool                      patternIsCaseSensitive)
    {
        // lambda that return position of pattern in string
        auto patternPosition = [this, &pattern, patternIsCaseSensitive](
                                   const std::string &str) {
            std::string compareStr =
                patternIsCaseSensitive ? str : toLower(str);
            size_t pos = compareStr.find(pattern);
            return pos == std::string::npos ? std::numeric_limits<size_t>::max()
                                            : pos;
        };

        std::sort(strings.begin(),
                  strings.end(),
                  [&patternPosition](const std::string &a, const std::string &b) {
                      return patternPosition(a) < patternPosition(b);
                  });
    }
};

std::string generateWord(int len)
{
    std::string word;
    for (int i = 0; i < len; ++i) {
        char c = rand() % 2 ? 'a' + rand() % 26
                            : 'A' + rand() % 26;    // Generate a random letter
        word.push_back(c);
    }
    return word;
}

int main()
{
    Trie trie;
    srand(time(0));    // seed random number generator

    // Insert 1M unique strings into the trie
    for (int i = 0; i < 1000000; ++i) {
        std::string str = generateWord(3);
        for (int j = 0; j < 4; ++j) {
            str += "_" + generateWord(3);
        }
        trie.insert(str);
    }

    while (true) {
        std::string search;
        int         limit;

        std::cout << "Enter search string and limit separated by space: ";
        std::cin >> search >> limit;

        std::vector<std::string> results;
        {
            ScopeBenchmark benchmark("Fuzzy search");
            results = trie.fuzzySearch(search, limit);
        }

        std::cout << "Fuzzy search results for " << search << " :\n";
        for (const auto &result : results) {
            std::cout << result << "\n";
        }

        // Wait for user input before continuing
        std::cout << "Press any key to continue...\n";
        std::cin.get();
        std::cin.get();    // To consume the newline character left in the input
                           // stream
    }

    return 0;
}


