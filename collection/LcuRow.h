#ifndef ROWCACHE_H_
#define ROWCACHE_H_

#include <QString>
#include <list>
#include <unordered_map>

/**
 * @class RowCache
 * @brief A cache for rows of data.
 *
 * This class implements a Least Recently Used (LRU) cache for rows of data.
 * Each row is identified by a key and contains a value. The keys are strings
 * and the values are of a templated type. The cache has a fixed capacity. When
 * the cache is full and a new row is added, the least recently used row is
 * removed.
 */
template <typename T>
class RowCache {
  public:
    /**
     * @brief Constructs a new RowCache.
     *
     * @param capacity The maximum number of rows that the cache can hold.
     */
    explicit RowCache(size_t capacity = 1000)
        : capacity_(capacity), default_value_(T()) {}

    /**
     * @brief Gets the value associated with a key.
     *
     * If the key exists in the cache, this method returns a pair containing
     * true and a reference to the value. If the key does not exist in the
     * cache, this method adds a new row with the key and a default value to the
     * cache, and returns a pair containing false and a reference to the default
     * value.
     *
     * @param key The key to get the value for.
     * @return A pair containing a boolean and a reference to a value. The
     * boolean is true if the key exists in the cache, and false otherwise. The
     *         reference is to the value associated with the key.
     */
    std::pair<bool, T &> Get(const QString &key) {
        auto it = cache_.find(key);
        if (it == cache_.end()) {
            // Item not found, add it
            Add(key, default_value_);
            it = cache_.find(key);
            return {false, it->second.first};
        }

        // Move the accessed item to the front
        keys_.splice(keys_.begin(), keys_, it->second.second);

        return {true, it->second.first};
    }

  private:
    /**
     * @brief Adds a new row with a key and a value to the cache.
     *
     * If the key already exists in the cache, this method removes the existing
     * row. If the cache is full, this method removes the least recently used
     * row. Then, this method adds a new row with the key and the value to the
     * cache.
     *
     * @param key The key of the row to add.
     * @param value The value of the row to add.
     */
    inline void Add(const QString &key, const T &value) {
        // If the key already exists in the cache, then remove it from the list
        auto it = cache_.find(key);
        if (it != cache_.end()) {
            keys_.erase(it->second.second);
        }

        // If the cache is full, remove the oldest item
        if (keys_.size() == capacity_) {
            QString oldest = keys_.back();
            keys_.pop_back();
            cache_.erase(oldest);
        }

        // Add the new item to the front
        keys_.push_front(key);
        cache_[key] = {value, keys_.begin()};
    }

    size_t capacity_;
    std::list<QString> keys_;
    std::unordered_map<QString, std::pair<T, std::list<QString>::iterator>>
        cache_;
    T default_value_;
};

#endif  // ROWCACHE_H_

