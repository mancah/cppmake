
import json

# Load the data from the JSON file
with open('/home/fsukri/trash/proxy_result.json') as f:
    data = json.load(f)

# Extract the list of server configurations
configs = data['resp_body']

# Initialize an empty list to hold the queries
queries = []

# Loop over each configuration
for config in configs:
    instances = config['instances']
    server_type = config['type']

    for instance in instances:
        # Extract the relevant data
        ip_address = instance['ipAddress']
        name = instance['name']
        init_port = instance['initPort']
        port = instance['port']

        # Generate the SQL query
        query = f"\"insert ServerConfig (IPAddress, Name, ServerType, InitPort, ServerPort) values ('{ip_address}', '{name}', '{server_type}', {init_port}, {port})\","

        # Add the query to the list
        queries.append(query)

# Print the queries
for query in queries:
    print(query)
