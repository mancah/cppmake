#ifndef INSTANCE_COUNTER_H
#define INSTANCE_COUNTER_H

#include <map>
#include <string>
#include <mutex>
#include <thread>
#include <fstream>
#include <chrono>
#include <iostream>

class InstanceCounter {
public:
    InstanceCounter(const std::string& name): name_(name) {
        std::lock_guard lock(mtx);
        counters[name]++;
    }

    ~InstanceCounter() {
        std::lock_guard lock(mtx);
        counters[name_]--;
    }

    static int getCount(const std::string& name) {
        std::lock_guard lock(mtx);
        return counters[name];
    }

    static void writeToFile()
    {
        std::lock_guard<std::mutex> lock(mtx);
        {
            //std::ofstream file("instance_counts.txt");
            for (const auto& pair : counters) {
                //file << pair.first << ": " << pair.second << "\n";
                std::cout << pair.first << ": " << pair.second << "\n";
            }

            std::cout << "File written\n\n\n";
        }
    }

private:
    static std::map<std::string, int> counters;
    static std::mutex mtx;
    std::string name_;

};

std::map<std::string, int> InstanceCounter::counters;
std::mutex InstanceCounter::mtx;

#endif  // INSTANCE_COUNTER_H


/**
#include "../collection/instance_counter.h"
#include <iostream>

class MyClass1 {
public:
    MyClass1() : counter("MyClass1") {}
    ~MyClass1() {}
private:
    InstanceCounter counter;
};

class MyClass2 {
public:
    MyClass2() : counter("MyClass2") {}
    ~MyClass2() {}
private:
    InstanceCounter counter;
};

int main() {
    {
        MyClass1 a, b, c;
        MyClass2 x, y;
        InstanceCounter::writeToFile();
    }

        InstanceCounter::writeToFile();


    return 0;
}
*/

