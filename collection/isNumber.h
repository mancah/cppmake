#include <QApplication>
#include <QDateTime>
#include <QString>
#include <QHash>

#include <QRegularExpression>

bool isNumber(const QString& str) {
    QRegularExpression re("^\\d+(\\.\\d+)?$");  // a regular expression for numbers and periods
    return re.match(str).hasMatch();
}

int main2 (int argc, char *argv[])
{
    QString str = "123";
    qDebug() << isNumber(str);

    QString sttr = "123.1263";
    qDebug() << isNumber(sttr);
    return 0;
}

