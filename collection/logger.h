#ifndef LOGGER_H_
#define LOGGER_H_

#include <atomic>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iomanip>
#include <mutex>
#include <string>
#include <thread>
#include <vector>

#include "scope_benchmark.h"

class Logger {
  public:
    static Logger& GetInstance() {
        static Logger instance;
        return instance;
    }

    void Log(const std::string& module, const std::string& message,
             const std::string& level = "INFO") {
        {
            std::lock_guard<std::mutex> lock(log_mutex_);

            std::stringstream ss;
            ss << std::this_thread::get_id() << GetDateTime() << " - "
               << "[" << module << "] - "
               << "[" << std::setw(7) << std::left << level << "] - "
               << message;
            log_messages_.push_back(ss.str());

            if (log_messages_.size() >= buffer_size_) {
                std::lock_guard<std::mutex> lock_taken(taken_mutex_);
                if (log_messages_.size() >= buffer_size_) {
                    log_taken_ = std::move(log_messages_);
                    log_messages_.clear();
                    if (!log_taken_.empty()) {
                        FlushLog();
                    }
                }
            }
        }
    }

    ~Logger() {
        try {
            std::lock_guard<std::mutex> lock(log_mutex_);
            std::lock_guard<std::mutex> lock_taken(taken_mutex_);
            log_taken_ = std::move(log_messages_);
            FlushLog();
        } catch (...) {
            std::cout << "Exception in ~Logger()" << std::endl;
        }
    }

    void SetBufferSize(size_t size) { buffer_size_ = size; }

  private:
    Logger() : log_file_("log.txt", std::ios_base::app), buffer_size_(5) {}

    Logger(const Logger&) = delete;
    Logger& operator=(const Logger&) = delete;

    void FlushLog() {
        // std::cout << "Flushing log..." << kCounter++ << std::endl;
        for (const auto& message : log_taken_) {
            log_file_ << message << std::endl;
        }
        log_taken_.clear();
    }

    std::string GetDateTime() {
        auto now = std::chrono::system_clock::now();
        auto now_c = std::chrono::system_clock::to_time_t(now);
        auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(
                      now.time_since_epoch()) %
                  1000;
        std::stringstream date_time;
        date_time << std::put_time(std::localtime(&now_c), "%d %B %Y %I:%M:%S")
                  << '.' << std::setfill('0') << std::setw(3) << ms.count()
                  << " " << std::put_time(std::localtime(&now_c), "%p");
        return date_time.str();
    }

    std::ofstream log_file_;
    std::mutex log_mutex_;
    std::mutex taken_mutex_;
    std::vector<std::string> log_messages_;
    std::vector<std::string> log_taken_;
    size_t buffer_size_;
    std::atomic<uint64_t> kCounter{0};
};

#endif  // LOGGER_H_


// main.cpp to test this
// #include <thread>
//
// #include "logger.h"
// #include "scope_benchmark.h"
//
// void log_operation(int num) {
//     for (int i = 0; i < num; ++i) {
//         Logger::GetInstance().Log("SubFunction", "This is a log message2", "WARNING");
//         Logger::GetInstance().Log("SubFunction", "This is a log message3", "ERROR");
//         Logger::GetInstance().Log("SubFunction", "This is a log message4", "DEBUG");
//         Logger::GetInstance().Log("SubFunction", "This is a log message5", "INFO");
//     }
// }
//
// int main(int argc, char* argv[]) {
//     ScopeBenchmark scope_benchmark("MainFunction");
//     int num = 1000000;
//     if (argc > 1) {
// 		num = std::atoi(argv[1]);
//     }
//
//     // Create multiple threads
//     std::thread t1(log_operation, num);
//     std::thread t2(log_operation, num);
//     std::thread t3(log_operation, num);
//     std::thread t4(log_operation, num);
//     std::thread t5(log_operation, num);
//
//     // Wait for all threads to finish
//     t1.join();
//     t2.join();
//     t3.join();
//     t4.join();
//     t5.join();
//
//     return 0;
// }

