#include <QJsonObject>
#include <QJsonArray>

namespace {

/**
 * @brief Parses a JSON object and assigns the value of a specified key to a
 * variable.
 *
 * This function checks if the JSON object contains the specified key. If the
 * key exists, the function assigns its value to the variable 'var'. The type of
 * 'var' determines how the value is extracted from the JSON object. The
 * function supports quint16, QVariantList, QString, and QList<quint16> types.
 * If the key does not exist or the extracted value is empty (for QVariantList,
 * QString, and QList<quint16>), the function logs an error message and returns
 * false.
 *
 * @tparam T The type of the variable to which the value is assigned. Supported
 * types are quint16, QVariantList, QString, and QList<quint16>.
 * @param json The JSON object from which the value is extracted.
 * @param path The path of the JSON file, used for error logging.
 * @param json_key The key whose value is to be extracted from the JSON object.
 * @param[out] var The variable to which the extracted value is assigned.
 * @return Returns true if the value is successfully extracted and assigned,
 * false otherwise.
 */
template <typename T>
bool GetValue(const QJsonObject &json, const QString &path,
              const QString &json_key, T &var) {
    if (!json.contains(json_key)) {
        qDebug() << "Error, " << json_key << " expected in:" << path;
        return false;
    }

    if constexpr (std::is_same_v<T, quint16>) {
        var = static_cast<quint16>(json[json_key].toInt());
    } else if constexpr (std::is_same_v<T, QVariantList>) {
        var = json[json_key].toArray().toVariantList();
        if (var.isEmpty()) {
            qDebug() << "Error, No " << json_key << " listed in:" << path;
            return false;
        }
    } else if constexpr (std::is_same_v<T, QString>) {
        var = json[json_key].toString();
        if (var.isEmpty()) {
            qDebug() << "Error," << json_key << " cannot be empty in:" << path;
            return false;
        }
    } else if constexpr (std::is_same_v<T, QList<quint16>>) {
        auto json_array = json[json_key].toArray();
        for (auto const &it : json_array) {
            var.append(static_cast<quint16>(it.toInt()));
        }

        if (var.isEmpty()) {
            qDebug() << "Error, No " << json_key << " listed in:" << path;
            return false;
        }
    } else {
        qDebug() << "Error, Unsupported type for GetValue when parsing "
                 << json_key << " in:" << path;
        return false;
    }
    return true;
}

} // namespace (anonymous)

/**
 * @brief function to read the configuration data from configuration
 * files in the directory provided
 * @param path - directory of the configuration files
 * @return boolean value to determine the validity of configuration data format
 */
bool ReadConfig(QString path) {
    quint16 http_port_;
    quint16 wss_port_;


    if (QFile file(path); file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QByteArray bytes = file.readAll();
        file.close();

        QJsonDocument doc = QJsonDocument::fromJson(bytes);
        PrettyPrintJSON(doc, "server.config.json");
        if (doc.isNull()) {
            return false;
        }

        auto all_config_json_ = doc.object();
        const auto &json = all_config_json_;

        return (GetValue(json, path, "initPort", http_port_) &&
                GetValue(json, path, "wssPort", wss_port_) &&
                GetValue(json, path, "xrtdbPort", server_config_.xrtdb_port) &&
                GetValue(json, path, "plugins", server_config_.plugins) &&
                GetValue(json, path, "name", server_config_.server_name));
    }

    qDebug() << "Error, unable to read:" << path;
    return false;
}


