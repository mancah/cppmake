#pragma once

#include <QEventLoop>
#include <QTimer>

namespace {

/**
 * @brief Creates an asynchronous block for a specified duration.
 *
 * This function uses a QEventLoop and QTimer to create a delay in the 
 * execution of the program. This is a workaround to prevent `busy waiting`,
 * which can be inefficient and consume unnecessary CPU resources.
 *
 * This is useful in scenarios where you want to pause the execution of 
 * your program without blocking the main thread, hence the name 'AsyncBlock'.
 *
 * @param ms The duration of the delay in milliseconds. The default value 
 * is 1000 ms (1 second).
 *
 * Example usage:
 * @code
 *     AsyncBlock(2000); // Creates a 2-second delay
 * @endcode
 */
void AsycnBlock(quint16 ms = 1000)
{
    QEventLoop loop;
    QTimer::singleShot(ms, &loop, &QEventLoop::quit);
    loop.exec();
}

}    // namespace

