#pragma once

#include <chrono>
#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

class ScopeBenchmark {
  public:
    ScopeBenchmark(std::string function_name = "", int new_line = 1) :
        m_start(std::chrono::high_resolution_clock::now()),
        m_function_name(function_name), m_new_line(new_line)
    { /* std::cout << "v1" << std::endl; */
    }

    ScopeBenchmark(const std::string &file_name,
                   int                creationLine,
                   const std::string &function_name) :
        m_start(std::chrono::high_resolution_clock::now()),
        m_file_name(file_name), m_function_name(function_name), m_new_line(1),
        m_creation_line(creationLine)
    { /* std::cout << "v2" << std::endl; */
    }

    ~ScopeBenchmark()
    {
        auto end = std::chrono::high_resolution_clock::now();
        auto time_taken =
            std::chrono::duration_cast<std::chrono::microseconds>(end - m_start)
                .count();

        if (m_creation_line) {
            auto line =
                m_file_name + ":" + std::to_string(m_creation_line) + " | ";

            std::cout << "bench_test => " << std::setw(10) << std::left << line
                      << std::setw(50) << std::left << m_function_name.c_str()
                      << " >>>>>>> " << std::setw(9) << std::right << time_taken
                      << " us"
                      << " (" << time_taken / 1000 << " ms)";
        } else {
            std::cout << std::setw(30)  << std::left << "bench_test | " << std::setw(20) << std::left
                      << m_function_name.c_str() << " >>>>>>> " << std::setw(9)
                      << std::right << time_taken << " us"
                      << " (" << time_taken / 1000 << " ms)";
        }

        while (m_new_line) {
            std::cout << std::endl;
            m_new_line--;
        }
    }

  private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start;
    std::string                                                 m_function_name;
    std::string                                                 m_file_name;
    int                                                         m_new_line = 0;
    int m_creation_line                                                    = 0;
};

