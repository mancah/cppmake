#ifndef JSONCLASSES_H2
#define JSONCLASSES_H2

#include <QByteArray>
#include <QString>
#include <QVector>
#include <future>
#include <iostream>
#include <vector>

namespace v2 {
class JsonArray;

class JsonObject {
public:
    JsonObject() : isFirstElement(true) {
        jsonString.reserve(1024); // Reserve initial capacity
        jsonString.append('{');
    }

    template <typename T>
    void insert(const QString& key, const T& value) {
        if (!isFirstElement) {
            jsonString.append(',');
        }
        jsonString.append('\"').append(key.toUtf8()).append("\":").append(QString::number(value).toUtf8());
        isFirstElement = false;
    }

    void insert(const QString& key, const QString& value) {
        if (!isFirstElement) {
            jsonString.append(',');
        }
        jsonString.append('\"').append(key.toUtf8()).append("\":\"").append(value.toUtf8()).append('\"');
        isFirstElement = false;
    }

    void insert(const QString& key, const char* value) {
        if (!isFirstElement) {
            jsonString.append(',');
        }
        jsonString.append('\"').append(key.toUtf8()).append("\":\"").append(value).append('\"');
        isFirstElement = false;
    }

    void insert(const QString& key, const JsonArray& jsonArray);

    void insert(const QString& key, const JsonObject& jsonObject) {
        if (!isFirstElement) {
            jsonString.append(',');
        }
        jsonString.append('\"').append(key.toUtf8()).append("\":").append(jsonObject.getJsonString());
        isFirstElement = false;
    }

    QByteArray getJsonString() const {
        QByteArray result = jsonString;
        result.append('}');
        return result;
    }

private:
    QByteArray jsonString;
    bool isFirstElement;
};

class JsonArray {
public:
    JsonArray() : isFirstElement(true) {
        jsonArrayString.reserve(1024); // Reserve initial capacity
        jsonArrayString.append('[');
    }

    template <typename T>
    void insert(const T& value) {
        if (!isFirstElement) {
            jsonArrayString.append(',');
        }
        jsonArrayString.append(QString::number(value).toUtf8());
        isFirstElement = false;
    }

    void insert(const QString& value) {
        if (!isFirstElement) {
            jsonArrayString.append(',');
        }
        jsonArrayString.append('\"').append(value.toUtf8()).append('\"');
        isFirstElement = false;
    }

    void insert(const char* value) {
        if (!isFirstElement) {
            jsonArrayString.append(',');
        }
        jsonArrayString.append('\"').append(value).append('\"');
        isFirstElement = false;
    }

    void insert(const JsonObject& jsonObject) {
        if (!isFirstElement) {
            jsonArrayString.append(',');
        }
        jsonArrayString.append(jsonObject.getJsonString());
        isFirstElement = false;
    }

    QByteArray getJsonArrayString() const {
        QByteArray result = jsonArrayString;
        result.append(']');
        return result;
    }

private:
    QByteArray jsonArrayString;
    bool isFirstElement;
};

inline void JsonObject::insert(const QString& key, const JsonArray& jsonArray) {
    if (!isFirstElement) {
        jsonString.append(',');
    }
    jsonString.append('\"').append(key.toUtf8()).append("\":").append(jsonArray.getJsonArrayString());
    isFirstElement = false;
}

inline void TestJsonClasses() {
    QByteArray jsonString;
    {
        JsonObject result;
        for (auto i = 0; i < 1000000; i++) {
            JsonObject jsonObject;
            jsonObject.insert("name", "John");
            jsonObject.insert("age", 25);
            jsonObject.insert("isStudent", true);

            JsonArray jsonArray;
            jsonArray.insert("apple");
            jsonArray.insert("banana");
            jsonArray.insert("cherry");

            jsonObject.insert("fruits", jsonArray);
            result.insert(QString::number(i), jsonObject);
        }

        jsonString = result.getJsonString();
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

inline JsonObject createJsonObject(int start, int end) {
    JsonObject result;
    for (auto i = start; i < end; i++) {
        JsonObject jsonObject;
        jsonObject.insert("name", "John");
        jsonObject.insert("age", 25);
        jsonObject.insert("isStudent", true);

        JsonArray jsonArray;
        jsonArray.insert("apple");
        jsonArray.insert("banana");
        jsonArray.insert("cherry");

        jsonObject.insert("fruits", jsonArray);
        result.insert(QString::number(i), jsonObject);
    }
    return result;
}

inline void TestJsonClassesAsync() {
    QByteArray jsonString;
    {
        const int numThreads = std::thread::hardware_concurrency();
        const int numObjects = 1000000;
        const int objectsPerThread = numObjects / numThreads;

        std::vector<std::future<JsonObject>> futures;
        for (int i = 0; i < numThreads; ++i) {
            int start = i * objectsPerThread;
            int end = (i == numThreads - 1) ? numObjects : start + objectsPerThread;
            futures.push_back(std::async(std::launch::async, createJsonObject, start, end));
        }

        JsonObject result;
        auto counter = 0;
        for (auto& future : futures) {
            JsonObject partialResult = future.get();
            result.insert(QString::number(counter++), partialResult);
        }

        jsonString = result.getJsonString();
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}
} // namespace v2

#endif // JSONCLASSES_H2

