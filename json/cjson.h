#ifndef C_JSON_H
#define C_JSON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <QByteArray>

class CJsonArray2;

class CJsonString2 {
public:
    CJsonString2();
    ~CJsonString2();
    CJsonString2(const CJsonString2 &other);
    CJsonString2 &operator=(const CJsonString2 &other);
    void append(const char *text);

    char *raw_data_;
    size_t size_;
    size_t capacity_;
};

class CJsonObject2 {
public:
    CJsonObject2();
    void insert(const char *key, const char *value);
    void insert(const char *key, int value);
    void insert(const char *key, bool value);
    void insert(const char *key, CJsonArray2 &array);
    void insert(const char *key, CJsonObject2 &obj);
    char *RawData();
    CJsonString2 &Data();
    QByteArray QByteArrayData();

private:
    CJsonString2 data_;
    bool first_element_;
};

class CJsonArray2 {
public:
    CJsonArray2();
    void insert(const char *value);
    void insert(CJsonObject2 &obj);
    void insert(CJsonArray2 &arr);
    char *RawData();
    CJsonString2 &Data();

private:
    CJsonString2 data_;
    bool first_element_;
};

void TestC2JsonClasses();

void AsyncTestC2JsonClasses();

#endif  // C_JSON_H

