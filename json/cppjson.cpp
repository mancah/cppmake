#include "cppjson.h"
#include <iostream>
#include <future>
#include <vector>

JsonObject::JsonObject() : isFirstElement(true) {
    jsonString.reserve(1024); // Reserve initial capacity
    jsonString = "{";
}

template <typename T>
void JsonObject::insert(const std::string& key, const T& value) {
    if (!isFirstElement) {
        jsonString += ",";
    }
    jsonString += "\"" + key + "\":" + std::to_string(value);
    isFirstElement = false;
}

void JsonObject::insert(const std::string& key, const std::string& value) {
    if (!isFirstElement) {
        jsonString += ",";
    }
    jsonString += "\"" + key + "\":\"" + value + "\"";
    isFirstElement = false;
}

void JsonObject::insert(const std::string& key, const char* value) {
    if (!isFirstElement) {
        jsonString += ",";
    }
    jsonString += "\"" + key + "\":\"" + value + "\"";
    isFirstElement = false;
}

void JsonObject::insert(const std::string& key, JsonArray& jsonArray) {
    if (!isFirstElement) {
        jsonString += ",";
    }
    jsonString += "\"" + key + "\":" + jsonArray.getJsonArrayString().toStdString();
    isFirstElement = false;
}

void JsonObject::insert(const std::string& key, JsonObject& jsonObject) {
    if (!isFirstElement) {
        jsonString += ",";
    }
    jsonString += "\"" + key + "\":" + jsonObject.getJsonString().toStdString();
    isFirstElement = false;
}

QByteArray JsonObject::getJsonString() {
    jsonString += "}";
    return QByteArray::fromStdString(jsonString);
}

JsonArray::JsonArray() : isFirstElement(true) {
    jsonArrayString.reserve(1024); // Reserve initial capacity
    jsonArrayString = "[";
}

template <typename T>
void JsonArray::insert(const T& value) {
    if (!isFirstElement) {
        jsonArrayString += ",";
    }
    jsonArrayString += std::to_string(value);
    isFirstElement = false;
}

void JsonArray::insert(const std::string& value) {
    if (!isFirstElement) {
        jsonArrayString += ",";
    }
    jsonArrayString += "\"" + value + "\"";
    isFirstElement = false;
}

void JsonArray::insert(const char* value) {
    if (!isFirstElement) {
        jsonArrayString += ",";
    }
    jsonArrayString += "\"" + std::string(value) + "\"";
    isFirstElement = false;
}

void JsonArray::insert(JsonObject& jsonObject) {
    if (!isFirstElement) {
        jsonArrayString += ",";
    }
    jsonArrayString += jsonObject.getJsonString().toStdString();
    isFirstElement = false;
}

QByteArray JsonArray::getJsonArrayString() {
    jsonArrayString += "]";
    return QByteArray::fromStdString(jsonArrayString);
}

void TestJsonClasses()
{
    QByteArray jsonString;
    {
        JsonObject result;
        for (auto i = 0; i < 1000000; i++) {
            JsonObject jsonObject;
            jsonObject.insert("name", "John");
            jsonObject.insert("age", 25);
            jsonObject.insert("isStudent", true);

            JsonArray jsonArray;
            jsonArray.insert("apple");
            jsonArray.insert("banana");
            jsonArray.insert("cherry");

            jsonObject.insert("fruits", jsonArray);
            result.insert(std::to_string(i), jsonObject);
        }

        jsonString = result.getJsonString();
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

JsonObject createJsonObject(int start, int end) {
    JsonObject result;
    for (auto i = start; i < end; i++) {
        JsonObject jsonObject;
        jsonObject.insert("name", "John");
        jsonObject.insert("age", 25);
        jsonObject.insert("isStudent", true);

        JsonArray jsonArray;
        jsonArray.insert("apple");
        jsonArray.insert("banana");
        jsonArray.insert("cherry");

        jsonObject.insert("fruits", jsonArray);
        result.insert(std::to_string(i), jsonObject);
    }
    return result;
}

void TestJsonClassesAsync() {
    QByteArray jsonString;
    {
        const int numThreads = std::thread::hardware_concurrency();
        const int numObjects = 1000000;
        const int objectsPerThread = numObjects / numThreads;

        std::vector<std::future<JsonObject>> futures;
        for (int i = 0; i < numThreads; ++i) {
            int start = i * objectsPerThread;
            int end = (i == numThreads - 1) ? numObjects : start + objectsPerThread;
            futures.push_back(std::async(std::launch::async, createJsonObject, start, end));
        }

        JsonObject result;
        auto counter = 0;
        for (auto& future : futures) {
            JsonObject partialResult = future.get();
            result.insert(std::to_string(counter++), partialResult);
        }

        jsonString = result.getJsonString();
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

