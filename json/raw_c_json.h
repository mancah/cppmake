#ifndef RAW_C_JSON_H
#define RAW_C_JSON_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct {
    char *data;
    size_t size;
    size_t capacity;
} CJsonString;

inline void CJsonString_init(CJsonString *str) {
    str->size = 0;
    str->capacity = 1024;
    str->data = (char *)malloc(str->capacity);
    str->data[0] = '\0';
}

inline void CJsonString_append(CJsonString *str, const char *text) {
    size_t text_len = strlen(text);
    if (str->size + text_len >= str->capacity) {
        str->capacity *= 2;
        str->data = (char *)realloc(str->data, str->capacity);
    }
    strcpy(str->data + str->size, text);
    str->size += text_len;
}

typedef struct {
    CJsonString jsonString;
    int isFirstElement;
} CJsonObject;

typedef struct {
    CJsonString jsonArrayString;
    int isFirstElement;
} CJsonArray;

inline void CJsonObject_init(CJsonObject *obj) {
    CJsonString_init(&obj->jsonString);
    CJsonString_append(&obj->jsonString, "{");
    obj->isFirstElement = 1;
}

inline void CJsonObject_insert(CJsonObject *obj, const char *key, const char *value) {
    if (!obj->isFirstElement) {
        CJsonString_append(&obj->jsonString, ",");
    }
    CJsonString_append(&obj->jsonString, "\"");
    CJsonString_append(&obj->jsonString, key);
    CJsonString_append(&obj->jsonString, "\":\"");
    CJsonString_append(&obj->jsonString, value);
    CJsonString_append(&obj->jsonString, "\"");
    obj->isFirstElement = 0;
}

inline void CJsonObject_insert_int(CJsonObject *obj, const char *key, int value) {
    char buffer[32];
    snprintf(buffer, sizeof(buffer), "%d", value);
    if (!obj->isFirstElement) {
        CJsonString_append(&obj->jsonString, ",");
    }
    CJsonString_append(&obj->jsonString, "\"");
    CJsonString_append(&obj->jsonString, key);
    CJsonString_append(&obj->jsonString, "\":");
    CJsonString_append(&obj->jsonString, buffer);
    obj->isFirstElement = 0;
}

void CJsonObject_insert_array(CJsonObject *obj, const char *key, CJsonArray *array);

inline void CJsonObject_finalize(CJsonObject *obj) {
    CJsonString_append(&obj->jsonString, "}");
}

inline void CJsonArray_init(CJsonArray *array) {
    CJsonString_init(&array->jsonArrayString);
    CJsonString_append(&array->jsonArrayString, "[");
    array->isFirstElement = 1;
}

inline void CJsonArray_insert(CJsonArray *array, const char *value) {
    if (!array->isFirstElement) {
        CJsonString_append(&array->jsonArrayString, ",");
    }
    CJsonString_append(&array->jsonArrayString, "\"");
    CJsonString_append(&array->jsonArrayString, value);
    CJsonString_append(&array->jsonArrayString, "\"");
    array->isFirstElement = 0;
}

inline void CJsonArray_finalize(CJsonArray *array) {
    CJsonString_append(&array->jsonArrayString, "]");
}

inline void CJsonObject_insert_array(CJsonObject *obj, const char *key, CJsonArray *array) {
    if (!obj->isFirstElement) {
        CJsonString_append(&obj->jsonString, ",");
    }
    CJsonString_append(&obj->jsonString, "\"");
    CJsonString_append(&obj->jsonString, key);
    CJsonString_append(&obj->jsonString, "\":");
    CJsonString_append(&obj->jsonString, array->jsonArrayString.data);
    obj->isFirstElement = 0;
}

inline void TestCJsonClasses() {
    CJsonString jsonString;
    clock_t start = clock();
    {
        CJsonObject result;
        CJsonObject_init(&result);
        for (int i = 0; i < 1000000; i++) {
            CJsonObject jsonObject;
            CJsonObject_init(&jsonObject);
            CJsonObject_insert(&jsonObject, "name", "John");
            CJsonObject_insert_int(&jsonObject, "age", 25);
            CJsonObject_insert(&jsonObject, "isStudent", "true");

            CJsonArray jsonArray;
            CJsonArray_init(&jsonArray);
            CJsonArray_insert(&jsonArray, "apple");
            CJsonArray_insert(&jsonArray, "banana");
            CJsonArray_insert(&jsonArray, "cherry");
            CJsonArray_finalize(&jsonArray);

            CJsonObject_insert_array(&jsonObject, "fruits", &jsonArray);
            CJsonObject_finalize(&jsonObject);

            char key[32];
            snprintf(key, sizeof(key), "%d", i);
            CJsonObject_insert(&result, key, jsonObject.jsonString.data);

            free(jsonObject.jsonString.data);
            free(jsonArray.jsonArrayString.data);
        }
        CJsonObject_finalize(&result);
        jsonString = result.jsonString;
    }
    clock_t end = clock();
    double time_taken = ((double)(end - start)) / CLOCKS_PER_SEC;
    printf("bench_test |    TestCJsonClasses                          >>>>>>>  %.0f ms\n", (time_taken*1000));
    // printf("Total Buffer Size: %.2fMB\n\n", jsonString.size / 1024.0 / 1024.0);
    free(jsonString.data);
}
#endif  // RAW_C_JSON_H

