#include "bytearrah.h"
#include "cjson.h"
#include "raw_c_json.h"
#include "cppjson.h"

#include <iostream>
#include <string>
#include <QByteArray>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QByteArray>

#include <chrono>
#include <iomanip>
#include <iostream>
#include <string>

class ScopeBenchmark {
  public:
    explicit ScopeBenchmark(const std::string &function_name) :
        m_function_name(function_name)
    { }

    ~ScopeBenchmark()
    {
        auto end = std::chrono::high_resolution_clock::now();
        auto time_taken =
            std::chrono::duration_cast<std::chrono::microseconds>(end - m_start)
                .count();

        std::cout << std::setw(16) << std::left << "bench_test | "
                  << std::setw(40) << std::left << m_function_name.c_str()
                  << " >>>>>>> " << std::setw(9) << std::right << time_taken
                  << " us" << " (" << time_taken / 1000 << " ms)";

        std::cout << std::endl;
    }

  private:
    std::chrono::time_point<std::chrono::high_resolution_clock> m_start =
        std::chrono::high_resolution_clock::now();
    std::string m_function_name;
};

void TestQJsonClasses()
{
    QByteArray jsonString;
    {
        ScopeBenchmark _1(__FUNCTION__);
        QJsonObject result;
        for (int i = 0; i < 1000000; i++) {
            QJsonObject jsonObject;
            jsonObject.insert("name", "John");
            jsonObject.insert("age", 25);
            jsonObject.insert("isStudent", true);

            QJsonArray jsonArray;
            jsonArray.append("apple");
            jsonArray.append("banana");
            jsonArray.append("cherry");

            jsonObject.insert("fruits", jsonArray);
            result.insert(QString::number(i), jsonObject);
        }

        QJsonDocument doc(result);
        jsonString = doc.toJson(QJsonDocument::Compact);
    }

    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

int main(int argc, char *argv[])
{
    for (auto i = 0; i < 10; i++)
    {
        // TestCJsonClasses();
        {
            ScopeBenchmark _1("TestC2JsonClasses");
            TestC2JsonClasses();
        }
        // {
        //     ScopeBenchmark _1("AsyncTestC2JsonClasses");
        //     AsyncTestC2JsonClasses();
        // }

        // {
        //     ScopeBenchmark _1("TestJsonClasses");
        //     TestJsonClasses();
        // }
        // {
        //     ScopeBenchmark _1("TestJsonClassesAsync");
        //     TestJsonClassesAsync();
        // }
        // {
        //     ScopeBenchmark _1("v2::TestJsonClasses");
        //     v2::TestJsonClasses();
        // }
        // {
        //     ScopeBenchmark _1("v2::TestJsonClassesAsync");
        //     v2::TestJsonClassesAsync();
        // }
    }
    return 0;
}

