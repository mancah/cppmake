#include "cjson.h"
#include <iostream>
#include <vector>
#include <future>

CJsonString2::CJsonString2() {
    size_ = 0;
    capacity_ = 1024;
    raw_data_ = (char *)malloc(capacity_);
    raw_data_[0] = '\0';
}

CJsonString2::~CJsonString2() {
    free(raw_data_);
}

CJsonString2::CJsonString2(const CJsonString2 &other) {
    size_ = other.size_;
    capacity_ = other.capacity_;
    raw_data_ = (char *)malloc(capacity_);
    memcpy(raw_data_, other.raw_data_, size_ + 1);
}

CJsonString2 &CJsonString2::operator=(const CJsonString2 &other) {
    if (this != &other) {
        free(raw_data_);
        size_ = other.size_;
        capacity_ = other.capacity_;
        raw_data_ = (char *)malloc(capacity_);
        memcpy(raw_data_, other.raw_data_, size_ + 1);
    }
    return *this;
}

void CJsonString2::append(const char *text) {
    size_t text_len = strlen(text);
    if (size_ + text_len >= capacity_) {
        capacity_ *= 2;
        raw_data_ = (char *)realloc(raw_data_, capacity_);
    }
    strcpy(raw_data_ + size_, text);
    size_ += text_len;
}

CJsonObject2::CJsonObject2() : first_element_(true) {
    data_.append("{");
}

void CJsonObject2::insert(const char *key, const char *value) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(key);
    data_.append("\":\"");
    data_.append(value);
    data_.append("\"");
    first_element_ = false;
}

void CJsonObject2::insert(const char *key, int value) {
    char buffer[32];
    snprintf(buffer, sizeof(buffer), "%d", value);
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(key);
    data_.append("\":");
    data_.append(buffer);
    first_element_ = false;
}

void CJsonObject2::insert(const char *key, bool value) {
    char buffer[2];
    snprintf(buffer, sizeof(buffer), "%d", value?1:0);
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(key);
    data_.append("\":");
    data_.append(buffer);
    first_element_ = false;
}

void CJsonObject2::insert(const char *key, CJsonArray2 &array) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(key);
    data_.append("\":");
    data_.append(array.RawData());
    first_element_ = false;
}

void CJsonObject2::insert(const char *key, CJsonObject2 &obj) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(key);
    data_.append("\":");
    data_.append(obj.RawData());
    first_element_ = false;
}

char* CJsonObject2::RawData() {
    data_.append("}");
    return data_.raw_data_;
}

CJsonString2 &CJsonObject2::Data() {
    data_.append("}");
    return data_;
}

QByteArray CJsonObject2::QByteArrayData() {
    return data_.raw_data_;
}

CJsonArray2::CJsonArray2() : first_element_(true) {
    data_.append("[");
}

void CJsonArray2::insert(const char *value) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append("\"");
    data_.append(value);
    data_.append("\"");
    first_element_ = false;
}

void CJsonArray2::insert(CJsonObject2 &obj) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append(obj.RawData());
    first_element_ = false;
}

void CJsonArray2::insert(CJsonArray2 &arr) {
    if (!first_element_) {
        data_.append(",");
    }
    data_.append(arr.RawData());
    first_element_ = false;
}

CJsonString2 &CJsonArray2::Data() {
    data_.append("]");
    return data_;
}

char* CJsonArray2::RawData() {
    data_.append("]");
    return data_.raw_data_;
}

inline CJsonObject2 GenerateJsonObject(int start, int end) {
    CJsonObject2 result;
    for (auto i = start; i < end; i++) {
        CJsonObject2 jsonObject;
        jsonObject.insert("name", "John");
        jsonObject.insert("age", 25);
        jsonObject.insert("isStudent", true);

        CJsonArray2 jsonArray;
        jsonArray.insert("apple");
        jsonArray.insert("banana");
        jsonArray.insert("cherry");

        jsonObject.insert("fruits", jsonArray);
        result.insert(std::to_string(i).c_str(), jsonObject);
    }
    return result;
}

inline CJsonArray2 GenerateJsonArray(int start, int end) {
    CJsonArray2 result;
    for (auto i = start; i < end; i++) {
        CJsonObject2 jsonObject;
        jsonObject.insert("name", "John");
        jsonObject.insert("age", 25);
        jsonObject.insert("isStudent", true);

        CJsonArray2 jsonArray;
        jsonArray.insert("apple");
        jsonArray.insert("banana");
        jsonArray.insert("cherry");

        jsonObject.insert("fruits", jsonArray);
        result.insert(jsonObject);
    }
    return result;
}



void TestC2JsonClasses() {
    auto jsonString = GenerateJsonObject(0, 1000000).QByteArrayData();
    std::cout << "Total Buffer Size: " << jsonString.size() / 1024.0 / 1024.0 << "MB\n" << std::endl;
}

void AsyncTestC2JsonClasses() {
    const int numThreads = std::thread::hardware_concurrency();
    // const int numObjects = 1000000;
    const int numObjects = 100;
    const int objectsPerThread = numObjects / numThreads;

    std::vector<std::future<CJsonArray2>> futures;
    for (int i = 0; i < numThreads; ++i) {
        int start = i * objectsPerThread;
        int end = (i == numThreads - 1) ? numObjects : start + objectsPerThread;
        futures.push_back(std::async(std::launch::async, GenerateJsonArray, start, end));
    }

    CJsonArray2 result;
    auto counter = 0;
    for (auto& future : futures) {
        CJsonArray2 json = future.get();
        result.insert(json);
    }
    std::cout << "Done Processing future" << std::endl;

    auto jsonString = result.Data();
    printf("Total Buffer Size: %.2fMB\n\n", jsonString.size_ / 1024.0 / 1024.0);
}

