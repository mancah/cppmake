#ifndef JSONCLASSES_H
#define JSONCLASSES_H

#include <QByteArray>
#include <string>

class JsonArray;
class JsonObject {
private:
    std::string jsonString;
    bool isFirstElement;

public:
    JsonObject();
    template <typename T>
    void insert(const std::string& key, const T& value);
    void insert(const std::string& key, const std::string& value);
    void insert(const std::string& key, const char* value);
    void insert(const std::string& key, JsonArray& jsonArray);
    void insert(const std::string& key, JsonObject& jsonObject);
    QByteArray getJsonString();
};

class JsonArray {
private:
    std::string jsonArrayString;
    bool isFirstElement;

public:
    JsonArray();
    template <typename T>
    void insert(const T& value);
    void insert(const std::string& value);
    void insert(const char* value);
    void insert(JsonObject& jsonObject);
    QByteArray getJsonArrayString();
};

void TestJsonClassesAsync();
void TestJsonClasses();

#endif // JSONCLASSES_H

