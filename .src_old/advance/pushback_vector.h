#include <chrono>
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

static int copy_counter = 0;
class Vertex {
  public:
    Vertex(float x, float y, float z) : x(x), y(y), z(z) { copy_counter++; }

    Vertex(const Vertex &vertex) : x(vertex.x), y(vertex.y), z(vertex.z)
    {
        copy_counter++;
        /* std::cout << "Copying Vertex: " << copy_counter++ << std::endl; */
    }

    static void reset_counter() { copy_counter = 0; }

    static void print()
    {
        std::cout << "Total copy constructor call: " << copy_counter++
                  << std::endl;
    }

  private:
    float x, y, z;
};

int main_test_push_back();
int main_test_emplace_back();

