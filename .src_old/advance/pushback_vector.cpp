
#include "pushback_vector.h"
#include "../benchmark/scope_benchmark.h"

static int64_t new_counter = 0;
void          *operator new(size_t size)
{
    new_counter++;
    return malloc(size);
}

int main_test_push_back()
{
    std::vector<Vertex> vertices;
    vertices.reserve(99999);
    Vertex::reset_counter();
    ScopeBenchmark _("push_back-99999", 4);
    new_counter = 0;
    for (auto i = 0; i < 99999; i++) {
        /* What push_back doing is passing the Vertex created in
         * main() scope into the copy constructor.
         *
         * By default vector will have a default size of memory.
         * When the following push_back dont have enough memory,
         * it will reallocate the vector datas into different
         * heap location. This reallocation will always reserve
         * a memory 1.5 of its previous reserved memory.
         *
         * Then copy everyting that we had into this new reserved memory
         * in heap. Yes it is a copy constructor for all the data.
         *
         * @note always use vertices.reserve(10) to make the size allocation
         *        big in one shot. This will reduce the unnecessary copy due to
         *        reallocation. We can always use a vector constructor if the
         * Vertex constructor is not expecting any args.
         *
         */
        vertices.push_back(Vertex(1 * i, 2 * i, 3 * i));
    }
    Vertex::print();
    std::cout << "Malloc call " << new_counter << std::endl;

    return 0;
}

int main_test_emplace_back()
{
    std::vector<Vertex> vertices;
    Vertex::reset_counter();
    ScopeBenchmark _("emplace_back-99999", 4);
    new_counter = 0;
    for (auto i = 0; i < 99999; i++) {
        /* emplace_back pass the args from vertices.emplace_back(args)
         * into Vertex constructo. This way the construction happen in the
         * heap and reduce the need to copy from main stack.
         * */
        vertices.emplace_back(1 * i, 2 * i, 3 * i);
    }
    Vertex::print();
    std::cout << "Malloc call " << new_counter << std::endl;

    return 0;
}

